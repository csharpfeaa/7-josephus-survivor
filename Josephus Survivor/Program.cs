﻿using System;

namespace Josephus_Survivor
{
    /**
     * https://www.codewars.com/kata/josephus-permutation/
     * the ancient historian Josephus: according to his tale, he and his 40 soldiers were trapped
     * in a cave by the Romans during a siege. Refusing to surrender to the enemy, they instead
     * opted for mass suicide, with a twist: they formed a circle and proceeded to kill one man every
     * three, until one last man was left (and that it was supposed to kill himself to end the act).
     *
     * JosephusSurvivor(7,3) => means 7 people in a circle;
     * one every 3 is eliminated until one remains
     * [1,2,3,4,5,6,7] - initial sequence
     * [1,2,4,5,6,7] => 3 is counted out
     * [1,2,4,5,7] => 6 is counted out
     * [1,4,5,7] => 2 is counted out
     * [1,4,5] => 7 is counted out
     * [1,4] => 5 is counted out
     * [4] => 1 counted out, 4 is the last element - the survivor!
     */
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("7 pers, a 3-a este eliminata");
            JosephusSurvivor(7, 3);

            Console.WriteLine("40 pers, a 3-a este eliminata");
            JosephusSurvivor(40, 3);

            Console.WriteLine("40 pers, a 4-a este eliminata");
            JosephusSurvivor(40, 4);

            Console.ReadKey();
        }

        public static void JosephusSurvivor(int numarPersoane, int nrEliminat)
        {
            int[] persoane = new int[numarPersoane];

            // populam vectorul cu persoane
            for (int i = 1; i <= numarPersoane; i++)
            {
                persoane[i - 1] = i;
            }

            int numaratoare = 1;

            while (persoane.Length > 1)
            {
                int[] persRamase = new int[0];

                for (int i = 0; i < persoane.Length; i++)
                {
                    if (numaratoare % nrEliminat != 0)
                    {
                        Array.Resize(ref persRamase, persRamase.Length + 1);
                        persRamase[persRamase.Length - 1] = persoane[i];
                    }

                    numaratoare++;
                }

                //AfiseazaVector(persRamase);
                persoane = persRamase;
            }

            Console.WriteLine($"Supravietuieste: {persoane[0]}");
        }

        static void AfiseazaVector(int[] v)
        {
            for (int i = 0; i < v.Length; i++)
            {
                Console.Write(v[i]);

                if (i < v.Length - 1)
                {
                    Console.Write(", ");
                }
            }

            Console.WriteLine();
        }
    }
}